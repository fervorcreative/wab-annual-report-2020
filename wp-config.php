<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Xh/wpA3LO1befvF63LbFbfUKhGOPKO/jXPT/zjB2xmfPSgiHDiJjhs32178pETntyw0n05P4rst1mLeiRljZxg==');
define('SECURE_AUTH_KEY',  'dt0hjkJwjK++tID+maIdtVChCvrJZQ/h7szOee266yHO4Lm5FFk504p3UF1FY/oE5VFy9csvcHIg3Jfwz3Hvaw==');
define('LOGGED_IN_KEY',    'vYfjoQURvSw/FP9ra//f4328VXX8gCKwRuBiHFlVn3BMEhtHeTst/nyDMjduxohKXGt4IXi9hMGqbHSIizzpzA==');
define('NONCE_KEY',        'jdedG0g44V5i7aGitCs8jNpzG0Azcg+aZcwH8T3xxKlbaBe1lRjlHbf1tZFWAFF1eUFkaetl00OaTTuEWrw5RQ==');
define('AUTH_SALT',        'b0ghN7WBh8gyLkrCv8AQZmOM9Yn5QduGk8wIp28S0iOOx6eigwSamnIZ8B3CQl8nY106BeGEV3MAstwhvpfIEA==');
define('SECURE_AUTH_SALT', 'yW0tdIbcw+jbnYs0ag3j9b8LajU9r1uUqBpCCpih55MsQihXNzCEPoKTlXMqIFGVx1Aeg1JBYpT9TrP4MRNvpQ==');
define('LOGGED_IN_SALT',   'x+3xb5i7SsjZlJrbLIKVbdJIZ+4RwQoeAPMxcUJPytLs2ozar62gxVdXlg/Z8JJVr2m8EGR66n8eq6wq4yA+DA==');
define('NONCE_SALT',       'vlgcSacovj1F1DZEMBs4Rz8A1KzB3y/AfmLQjvwgiHBDuWrVdV0O9Dxskgt3Z62/QqKiMLjVrCf1uCQjvHEF/g==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
